first_overdraft = """def get_date_and_value_of_first_overdraft1(transactions, dates):
    transactions_and_dates = list(zip(transactions, dates))
    transactions_and_dates.sort(key=lambda x: x[1])
    running_total = 0
    for amount, date in transactions_and_dates:
        running_total += amount
        if running_total < 0:
            return running_total, date
get_date_and_value_of_first_overdraft1(transactions, dates)
"""

credit_check = """def check_credit_request(rating, is_commercial_customer, additional_credit, current_credit, limit):
    if is_commercial_customer:
        has_good_rating = rating.upper() in ["A", "B"]
        return has_good_rating
    else:
        free_limit = limit - (current_credit + additional_credit)
        return free_limit >= 0
"""
